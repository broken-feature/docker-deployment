# Déploiement de Broken Feature

L'objectif de ce répertoire est de fournir un moyen simple pour déployer une instance de Broken Feature.

## Prérequis

Un environnement Docker et Docker compose fonctionnel (capable d'utiliser des fichiers compose version 3.5 ou supérieure) sur une machine amd64 ou arm64.

## Installation

### Cloner le répertoire

Exécuter:

```
git clone https://gitlab.com/broken-feature/docker-deployment.git broken-feature
```

### Configuration

Dans le nouveau dossier `broken-feature`, copier le fichier `.env.example` vers un nouveau fichier `.env`.

Dans le fichier `.env`, spécifier au moins le mot de passe pour l'utilisateur courant à la ligne suivante :

```
DB_PASSWORD=
```

Dans le fichier `docker-compose.yml`, spécifier le mot de passe pour l'utilisateur `root` à la ligne suivante :

``` Yaml
MYSQL_ROOT_PASSWORD: secret # <-- Change this before usage in production
```

## Lancement

Exécuter :

```
docker-compose up -d
```

L'application est désormais accessible sur le port `8080`.

## Extras

### Connexions sécurisées (https)

Il n'y a pas de moyens directement mis en place pour la gestion du https, cette dernière étant à la charge de l'infrastructure sur laquelle sera hébérgée l'instance de Broken Feature.

Si l'infrastructure prend en charge les connexions sécurisées, elle doit attacher un en-tête `X-Forwarded-Proto` indiquant le protocole utilisé (`http` ou `https`) lors de sa connexion à l'application.